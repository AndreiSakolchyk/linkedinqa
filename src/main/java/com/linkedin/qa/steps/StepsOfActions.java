package com.linkedin.qa.steps;


import com.linkedin.qa.businessObject.User;
import com.linkedin.qa.pages.HomeProfilePage;
import com.linkedin.qa.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class StepsOfActions {

    private WebDriver driver;

    public void initDriver() {
        System.setProperty("webdriver.firefox.profile", "Selenium");
        this.driver = new FirefoxDriver();
        this.driver.manage().window().maximize();
    }

    public void destroyDriver() {
        this.driver.quit();
    }

    public String doLogin(User linkedinUser) {
        LoginPage login = new LoginPage(driver);

        login.openPage();
        login.setUserEmail(linkedinUser.getEmail());
        login.setPassword(linkedinUser.getPassword());
        login.submitLogin();

        return this.driver.getCurrentUrl();
    }

    public String changeJobTitle(String jobTitle) throws InterruptedException {
        HomeProfilePage profile = new HomeProfilePage(driver);

        profile.openPage();
        profile.openJobTitle();
        profile.setJobTitle(jobTitle);
        profile.submitJobTitle();

        return profile.getJobTitle();
    }

}
