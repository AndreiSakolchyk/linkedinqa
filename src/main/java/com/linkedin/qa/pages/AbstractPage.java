package com.linkedin.qa.pages;

import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {

    protected WebDriver driver;

    public abstract void openPage();

    public abstract void init();

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

}
