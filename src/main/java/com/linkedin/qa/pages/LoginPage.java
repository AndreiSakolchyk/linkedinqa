package com.linkedin.qa.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.awt.print.PageFormat;

public class LoginPage extends AbstractPage {

    private String PAGE_URL = "https://www.linkedin.com/uas/login";

    @FindBy(xpath = ".//input[@id='session_key-login']")
    private WebElement userEmail;

    @FindBy(xpath = ".//input[@id='session_password-login']")
    private WebElement password;

    @FindBy(xpath = ".//input[@id='btn-primary']")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void openPage() {
        driver.get(PAGE_URL);
        init();
    }

    @Override
    public void init() {
        PageFactory.initElements(this.driver, this);
    }

    public void setUserEmail(String userEmail) {
        this.userEmail.click();
        this.userEmail.sendKeys(Keys.chord(Keys.CONTROL, "a"), userEmail);
    }

    public void setPassword(String password) {
        this.password.sendKeys(password);
    }

    public void submitLogin() {
        this.loginButton.click();
    }

}
