package com.linkedin.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class HomeProfilePage extends AbstractPage{

    private String PAGE_URL = "https://www.linkedin.com/profile/edit";

    @FindBy(xpath = ".//*[@id='headline']/div[1]/p")
    private WebElement jobTitle;

    public HomeProfilePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void openPage() {
        driver.get(PAGE_URL);
        System.out.println(driver.getCurrentUrl());
        init();
    }

    @Override
    public void init() {
        PageFactory.initElements(this.driver, this);
    }

    public void openJobTitle() {
        jobTitle.click();
    }

    public String getJobTitle() {
        String result = driver.findElement(By.xpath(".//*[@id='headline']/div[1]/p")).getText();
        return result;
    }

    public void setJobTitle(String jobTitle) {
        WebElement inputJobTitle = driver.findElement(By.xpath(".//*[@id='headline-editHeadlineForm']"));
        inputJobTitle.click();
        inputJobTitle.sendKeys(Keys.chord(Keys.CONTROL, "a"), jobTitle);
    }

    public void submitJobTitle() throws InterruptedException {
        WebElement button = driver.findElement(By.xpath(".//*[@id='headline-edit']/p[2]/input"));
        //driver.manage().timeouts().implicitlyWait(6000, TimeUnit.SECONDS);
        button.click();
    }

}
