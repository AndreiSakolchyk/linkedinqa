package com.linkedin.qa;

import com.linkedin.qa.businessObject.User;
import com.linkedin.qa.steps.StepsOfActions;
import com.linkedin.qa.utils.StringGenerator;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LinkedinAutomationQA {

    private StepsOfActions steps;
    private User linkedinUser;

    @BeforeTest
    public void initData() {
        this.steps = new StepsOfActions();
        this.steps.initDriver();
        this.linkedinUser = new User("rahahop@mswork.ru", "4(Gx*WL0yfPQ0%6*");
        this.steps.doLogin(this.linkedinUser);
    }

    @AfterTest
    public void destroyDate() {
        this.steps.destroyDriver();
    }

    /*@Test
    public void testLogin() {
        String resultUrl = this.steps.doLogin(this.linkedinUser);
        System.out.println("--->" + resultUrl);
        Assert.assertEquals(resultUrl.contains("home"), true);
    }*/

    @Test
    private void testProfile_changeJobTitle() throws InterruptedException {
        String title = StringGenerator.getString();
        String result = this.steps.changeJobTitle(title);
        
        Assert.assertEquals(result.contains(title), true);
    }


}
